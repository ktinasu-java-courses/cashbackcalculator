package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CashbackServiceTest {
    @Test
    void shouldCalculate() {
        CashbackService service = new CashbackService();
        int money = 203_78;
        int expected = 2;

        int actual = service.calculate(money);

        assertEquals(expected, actual);
    }

    @Test
    void shouldCalculateForMoreThanMaxCashback() {
        CashbackService service = new CashbackService();
        int money = 310000_00;
        int expected = 3000;

        int actual = service.calculate(money);

        assertEquals(expected, actual);
    }

    @Test
    void shouldCalculateForLessThanMinCashback() {
        CashbackService service = new CashbackService();
        int money = 98_60;
        int expected = 0;

        int actual = service.calculate(money);

        assertEquals(expected, actual);
    }
}
