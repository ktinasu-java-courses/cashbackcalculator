package org.example.service;

public class CashbackService {
    public int calculate(int money) {
        int maxCashback = 3000;
        int minCashback = 1;
        double percentage = 0.01;
        int sum = (int) (money / 100 * percentage);

        if (sum > maxCashback) {
            return (maxCashback);
        }

        if (sum < minCashback) {
            return 0;
        }

        return sum;
    }
}




